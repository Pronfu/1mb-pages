# 1MB Pages #

Webpages are getting bigger and bigger, website owners don't care about how big they get. To show off how big I took some of the top (or most well-known) css frameworks and put them each on their own page. Then compared the page size to each other. Some of these you won't be surpised, and some of them you will be surpised.

## Installation ##
Either clone the repo `git clone https://bitbucket.org/Pronfu/1mb-pages.git` or [download it](https://bitbucket.org/Pronfu/1mb-pages/get/42b25691b7d6.zip). Once in a folder then move it to wherever you want to edit it, edit it, then copy it to your server (usually in public_html). There you have it.

## Live Site ##
[https://projects.gregoryhammond.ca/1mb-pages/](https://projects.gregoryhammond.ca/1mb-pages/)

## License ##
[Unlicense](https://unlicense.org/)